//
//  UserCardsViewController.swift
//  PracticalTask
//
//  Created by Jaykhodiyar on 02/04/20.
//  Copyright © 2020 Jaykhodiyar. All rights reserved.
//

import UIKit
import SDWebImage
struct structureDictionary {
    var avatar : String!
    var name : String!
}

class UserCardsViewController: UIViewController {
    @IBOutlet weak var usercardCollectionView: UICollectionView!
    var arydic = [structureDictionary]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    arydic.append((structureDictionary(avatar:"https://vignette.wikia.nocookie.net/characterprofile/images/7/75/Mario.png", name: "OG Mario"))
        )
        arydic.append((structureDictionary(avatar:"https://vignette.wikia.nocookie.net/characterprofile/images/c/c4/Fire_mario.png", name: "Fire Mario"))
        )
        arydic.append((structureDictionary(avatar:"https://vignette.wikia.nocookie.net/characterprofile/images/9/9a/Tanooki_mario.png", name: "Raccoon Mario"))
        )
        arydic.append((structureDictionary(avatar:"https://vignette.wikia.nocookie.net/characterprofile/images/d/d1/Invincible_Mario.png", name: "Rainbow Mario"))
        )
        arydic.append((structureDictionary(avatar:"https://vignette.wikia.nocookie.net/characterprofile/images/8/83/1452500936178.png", name: "Cloud Mario"))
        )
        arydic.append((structureDictionary(avatar:"https://vignette.wikia.nocookie.net/characterprofile/images/8/87/SML2RabbitMario.png", name: "Rabbit Mario"))
        )
        arydic.append((structureDictionary(avatar:"https://vignette.wikia.nocookie.net/characterprofile/images/2/27/250px-Dr_Mario_-_Dr_Mario_Miracle_Cure.png", name: "Doctor Mario"))
        )
        arydic.append((structureDictionary(avatar:"https://vignette.wikia.nocookie.net/characterprofile/images/5/50/1452500917700.png", name: "Bee Mario"))
        )
        arydic.append((structureDictionary(avatar:"https://vignette.wikia.nocookie.net/characterprofile/images/b/b4/Penguin_Mario.png", name: "Penguin Mario"))
        )
        arydic.append((structureDictionary(avatar:"https://vignette.wikia.nocookie.net/characterprofile/images/5/55/Frog_mario.png", name: "Frog Mario"))
        )
        arydic.append((structureDictionary(avatar:"https://vignette.wikia.nocookie.net/characterprofile/images/9/9d/860px-Squirrel_Mario_NSMBU.png", name: "Squirrel Mario"))
        )
        arydic.append((structureDictionary(avatar:"https://vignette.wikia.nocookie.net/characterprofile/images/4/47/ShellMario.png", name: "Turtle Mario"))
        )
        arydic.append((structureDictionary(avatar:"https://vignette.wikia.nocookie.net/characterprofile/images/3/32/684px-WingMarioSM64DS.png", name: "Flying Mario"))
        )
        arydic.append((structureDictionary(avatar:"https://vignette.wikia.nocookie.net/characterprofile/images/2/23/Metal_Mario.png", name: "Metal Mario"))
            
        )
        arydic.append((structureDictionary(avatar:"https://vignette.wikia.nocookie.net/characterprofile/images/6/63/Ice_Mario_2.png", name: "Ice Mario")))
       
        usercardCollectionView.register(UINib(nibName: "UserCardIpadCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "UserCardIpadCollectionViewCell")
        usercardCollectionView.register(UINib(nibName: "UserCardsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "UserCardsCollectionViewCell")
        usercardCollectionView.reloadData()
        // Do any additional setup after loading the view.*/
    }
}


extension UserCardsViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arydic.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if UIDevice.current.userInterfaceIdiom == .pad{
            let cell : UserCardIpadCollectionViewCell = usercardCollectionView.dequeueReusableCell(withReuseIdentifier: "UserCardIpadCollectionViewCell", for: indexPath) as! UserCardIpadCollectionViewCell
            let dic = arydic[indexPath.row]
            let gradientLayer: CAGradientLayer = CAGradientLayer()
            // Set frame of gradient layer.
            gradientLayer.frame = cell.viewTopBackground.bounds
            // Color at the top of the gradient.
            let topColor: CGColor = UIColor(displayP3Red: 117.0/255.0, green: 54.0/255.0, blue: 249.0/255.0, alpha: 1.0).cgColor
            // Color at the bottom of the gradient.
            let bottomColor: CGColor = UIColor(displayP3Red: 57.0/255.0, green: 184.0/255.0, blue: 244.0/255.0, alpha: 1.0).cgColor
            gradientLayer.colors = [bottomColor, topColor]
            // Set start point.
            gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
            // Set end point.
            gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
            // Insert gradient layer into view's layer heirarchy.
            cell.viewTopBackground.layer.insertSublayer(gradientLayer, at: 0)
            
            cell.imageViewLogo.setCornerRadiusImage(value: cell.imageViewLogo.frame.size.height / 2, borderWidth: 3.0, borderColor: UIColor.white)
            cell.imageViewLogo.sd_setImage(with: URL(string: dic.avatar), placeholderImage: nil)
            cell.labelName.text = dic.name
            return cell
        }
        else{
            let cell : UserCardsCollectionViewCell = usercardCollectionView.dequeueReusableCell(withReuseIdentifier: "UserCardsCollectionViewCell", for: indexPath) as! UserCardsCollectionViewCell
            
            let dic = arydic[indexPath.row]
            
            let gradientLayer: CAGradientLayer = CAGradientLayer()
            // Set frame of gradient layer.
            gradientLayer.frame = cell.viewTopBackground.bounds
            // Color at the top of the gradient.
            let topColor: CGColor = UIColor(displayP3Red: 117.0/255.0, green: 54.0/255.0, blue: 249.0/255.0, alpha: 1.0).cgColor
            // Color at the bottom of the gradient.
            let bottomColor: CGColor = UIColor(displayP3Red: 57.0/255.0, green: 184.0/255.0, blue: 244.0/255.0, alpha: 1.0).cgColor
            gradientLayer.colors = [bottomColor, topColor]
            // Set start point.
            gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
            // Set end point.
            gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
            // Insert gradient layer into view's layer heirarchy.
            cell.viewTopBackground.layer.insertSublayer(gradientLayer, at: 0)
            
            cell.imageViewLogo.setCornerRadiusImage(value: cell.imageViewLogo.frame.size.height / 2, borderWidth: 3.0, borderColor: UIColor.white)
            cell.imageViewLogo.sd_setImage(with: URL(string: dic.avatar), placeholderImage: nil)
            
            cell.labelName.text = dic.name
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad{
            return CGSize(width: (self.view.frame.size.width - 20) / 3, height: 130)
        }
        else{
            return CGSize(width: self.view.frame.size.width - 20, height: 120)
        }
    }
    
    
}
