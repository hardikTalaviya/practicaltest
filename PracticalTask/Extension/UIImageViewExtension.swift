//
//  UIImageViewExtension.swift
//  PracticalTask
//
//  Created by Jaykhodiyar on 02/04/20.
//  Copyright © 2020 Jaykhodiyar. All rights reserved.
//

import Foundation
import UIKit
import CoreGraphics
extension UIImageView {
    
    
    func setCornerRadiusImage(value:CGFloat, borderWidth:CGFloat? = nil, borderColor:UIColor? = nil) {
        self.layer.masksToBounds = true
        self.layer.cornerRadius = value
        
        guard let borderWidth = borderWidth, let borderColor = borderColor else { return }
        
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = borderColor.cgColor
    }
}
