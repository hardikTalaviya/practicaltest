//
//  UserCardsCollectionViewCell.swift
//  PracticalTask
//
//  Created by Jaykhodiyar on 02/04/20.
//  Copyright © 2020 Jaykhodiyar. All rights reserved.
//

import UIKit

class UserCardsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var viewTopBackground: UIView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var imageViewLogo: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
